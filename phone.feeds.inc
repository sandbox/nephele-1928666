<?php
/**
 * @file
 * Implements Feeds mapping API
 */

/**
 * Implements hook_feeds_processor_targets_alter().
 */
function phone_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {
    $info = field_info_field($name);
    if ($info['type'] == 'phone') {
      $targets[$name] = array(
        'name' => t('!label - single field', array('!label' => check_plain($instance['label']))),
        'callback' => 'phone_feeds_set_target',
        'description' => t('The @label field.  Use if the entire number (country code, number, and extension) are provided in a single field.', array('@label' => $instance['label'])),
      );
      $targets[$name . ':country_code'] = array(
        'name' => t('!label - country code', array('!label' => check_plain($instance['label']))),
        'callback' => 'phone_feeds_set_target',
        'description' => t('The country code for a @label field, providing the two-letter code of the phone number\'s country.', array('@label' => $instance['label'])),
      );
      $targets[$name . ':number'] = array(
        'name' => t('!label - phone number', array('!label' => check_plain($instance['label']))),
        'callback' => 'phone_feeds_set_target',
        'description' => t('The national number for a @label field, without any country information or extension.', array('@label' => $instance['label'])),
      );
      $targets[$name . ':extension'] = array(
        'name' => t('!label - extension', array('!label' => check_plain($instance['label']))),
        'callback' => 'phone_feeds_set_target',
        'description' => t('The extension for a @label field.', array('@label' => $instance['label'])),
      );
    }
  }
}

/**
 * Callback for mapping. Here is where the actual mapping happens.
 *
 * When the callback is invoked, $target contains the name of the field the
 * user has decided to map to and $value contains the value of the feed item
 * element the user has picked as a source.
 */
function phone_feeds_set_target($source, $entity, $target, $value) {
  if (empty($value)) {
    return;
  }

  // Handle non-multiple value fields.
  if (!is_array($value)) {
    $value = array($value);
  }

  // Iterate over all values.
  $i = 0;
  $info = field_info_field($target);
  if (strpos($target, ':')===FALSE) {
    $field_name = $target;
    $sub_field = NULL;
  }
  else {
    list($field_name, $sub_field) = explode(':', $target);
  }

  // We will call this multiple times, preserve existing values.
  $field = empty($entity->{$field_name}) ? array() : $entity->{$field_name};

  foreach ($value as $v) {
    if (!is_array($v) && !is_object($v)) {
      if (!isset($sub_field)) {
      // Convert whole number into components.
        $item = _phone_migrate_phone_number($v);
        foreach ($item as $sub_field => $sub_value) {
          $field[LANGUAGE_NONE][$i][$sub_field] = $sub_value;
        }
      }
      else {
        if ($sub_field == 'country_code') {
          $country_info = phone_countrycodes($v);
          if (!empty($country_info)) {
            $v = $country_info['code'];
          }
          else {
            $v = NULL;
          }
        }
        else {
          // Strip all non-numbers from number/extension.
          $v = preg_replace('/[^\d]/', '', $v);
        }

        $field[LANGUAGE_NONE][$i][$sub_field] = $v;
      }
    }
    $i++;
    if ($info['cardinality'] != FIELD_CARDINALITY_UNLIMITED && $i >= $info['cardinality']) {
      break;
    }
  }
  $entity->{$field_name} = $field;
}
