Description
-----------
This module provides a phone field type.

This is currently a sandbox project to develop version phone-7.x-2.x
of the phone module, intended to merge cck_phone and phone into a
single project, and use libphonenumber-for-PHP to provide
comprehensive support for country-specific validation and
formatting.

Features
--------
* Validation of phone numbers.  Country-specific validation for more
  than 200 countries.  Country-specific validation can also
  optionally be turned off for any phone number field.
* Formating of phone numbers, according to several standard formats
  (E123, E164, RFC3966), including country-specific formatting
  preferences.
* Any phone number field can support as many countries as needed
  within a single widget.
* Phone numbers can be edited using either a single text input
  (combining the phone country code, number, and extension), or
  using separate inputs for each component.
* Optionally allow extensions and type (home, work, fax) to
  be specified for each phone number.
* Support for tokens.
* Support for the entity module.
* Support for the feeds module.
* <s>Support for the migrate module.</s> -- in progress
* IPhone support
* Adds phone and phone_combo form elements that can be used
  by other modules.

Prerequisites
-------------
The phone module requires that the fields module (part of drupal
core) be enabled.

It also requires the libphonenumber-for-PHP library.

Currently (Feb. 26, 2013), the recommended source of the library is:
  https://github.com/Nephele/libphonenumber-for-PHP

However, this is hopefully a temporary solution.  The original
source of the library, and hopefully the long-term location of
the library is:
  https://github.com/davideme/libphonenumber-for-PHP
As of Feb. 26, 2013, the davideme version of the library contains
several bugs (including one that makes US and all North America
phone numbers invalid, and another that makes extensions unusable).
Until those bugs are fixed in the davideme version of the library,
the Nephele version has been provided containing fixes for all
known bugs.

The contents of the libphonenumber-for-PHP library should be
placed within the drupal folder, in the directory:
  sites/all/libraries/libphonenumber-for-PHP/

Updating From Previous Versions
-------------------------------
This module includes support for updating from earlier versions of
both the phone and cck_phone modules.

* Drupal 6 migrations (phone-6.x and cck_phone-6.x) are done via the
  CCK content_migrate module, used to migrate all Drupal 6 fields
  to Drupal 7.  See http://drupal.org/project/cck.
* Drupal 7 phone (phone-7.x-1.x) migration is handled by the standard
  module update mechanism, i.e., by running update.php after installing
  the latest module version.
* Drupal 7 cck_phone (cck_phone-7.x-1.x) migration is handled by the
  CCK Phone Converter (phone_cck_convert) sub-module.  This is
  included as part of the standard phone module distribution, but
  needs to be enabled separately on the Modules page.  Once enabled,
  go to admin/config/content/phone-cck-convert to do the migration.

The update will match your existing fields' options as
closely as possible.  However, you are then free to configure
the fields to take advantage of the new features offered
by this module.

New options for how to edit phone fields are available
under 'Manage Fields' for your content type(s).  New options
for how to display phone fields are available under
'Manage Display' for your content type(s).

Module Status
-------------
At the moment, this module is still under development.

Many of the desired features have been incorporated into the
module, but more testing of all aspects of the module is
needed.

The most important issues right now are:
* Making sure that existing users can upgrade to this version is the
  top priority.  All upgrade/migration options (see Updating From Previous
  Versions) need extensive testing.  Basic testing has been done for
  cck_phone-7.x-1.x and phone-7.x-1.x upgrades, but no testing has
  been done yet for upgrades from drupal 6 (either cck_phone-6.x or
  phone-6.x).
* The libphonenumber-for-PHP module contains bugs.  The known bugs
  have been fixed in a new copy of the module (https://github.com/Nephele/libphonenumber-for-PHP)
  but others may still exist (and there are active discussions
  about how to fix some of the bugs).
* Making the libphonenumber library optional.  Given concerns about
  bugs and the extra complication introduced by requiring an external
  library, it seems desirable to make it possible to use the phone
  module without installing libphonenumber -- in particular if doing
  so would encourage existing users to migrate to the new module
  more quickly.  There are at least two options:
  a) If the libphonenumber library is not present, nearly all
     validation and formatting options are disabled.  The input
     fields would be little more than standard text input fields.
  b) Allow some or all of the country-specific includes from
     phone and/or cck_phone to be ported into phone-7.x-2.x.  These
     files would only be used if libphonenumber is not available.
     This would maximize backwards-compatibility for existing users,
     but would make maintaining the module more difficult.
  At the moment, making libphonenumber optional has been taken into
  account in some places (e.g., overall design and schema), but most
  of the work still needs to be done.

Other features that are known to be incomplete:
* Support for the migrate module.
* Support for the libraries modules, to allow greater flexibility
  in managing the libphonenumber-for-PHP library.
* Simpletest testing functions.
* Greater support for phone number types, in particular support
  for all hcard types and support for displaying phone numbers
  using hcard microformats.

  
