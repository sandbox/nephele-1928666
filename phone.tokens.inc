<?php

/**
 * @file
 * Implement phone number tokens, via core token module.
 */

/**
 * Implements hook_token_info().
 */
function phone_token_info() {
  $types = array(
    'phone-field-value' => array(
      'name' => t('Phone number field values'),
      'description' => t('Tokens for displaying phone number field values.'),
      'needs-data' => 'phone-field-value',
      'field-value' => TRUE,
    ),
  );

  $phone = array();
  $phone['number'] = array(
    'name' => t('Raw phone number'),
    'description' => t("The unformatted value of the phone number, without country code or extension."),
  );
  $phone['country_code'] = array(
    'name' => t('Phone country code (two-letter-code)'),
  );
  $phone['extension'] = array(
    'name' => t('Phone extension'),
  );
  $phone['type'] = array(
    'name' => t('Phone number type'),
  );
  $phone['country_number'] = array(
    'name' => t('Phone country number'),
  );
  $phone['country_name'] = array(
    'name' => t('Phone country name'),
  );
  $phone['formatted-local'] = array(
    'name' => t('Phone number - local format'),
    'description' => t('The phone number, including country code and extension, formatted using the phone_local formatter.'),
  );
  $phone['formatted-global'] = array(
    'name' => t('Phone number - global format'),
    'description' => t('The phone number, including country code and extension, formatted using the phone_global formatter.'),
  );
  $phone['formatted-e164'] = array(
    'name' => t('Phone number - E164 format'),
    'description' => t('The phone number, including country code and extension, formatted using the phone_e164 formatter.'),
  );
  $phone['formatted-rfc3966'] = array(
    'name' => t('Phone number - RFC3966 format'),
    'description' => t('The phone number, including country code and extension, formatted using the phone_rfc3966 formatter.'),
  );

  return array(
    'types' => $types,
    'tokens' => array('phone-field-value' => $phone),
  );
}

/**
 * Implements hook_tokens().
 */
function phone_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'phone-field-value' && !empty($data['phone-field-value'])) {
    foreach ($tokens as $name => $original) {
      if (drupal_substr($name, 0, 10)=='formatted-') {
        $format = 'phone_' . drupal_substr($name, 10);
        // @todo: how to get settings needed by _phone_get_formatted_number?
        $replacements[$original] = _phone_get_formatted_number($data['phone-field-value'], $format, array());
      }
      elseif ($name=='country_name' || $name=='country_number') {
        // Fallback to empty entry
        $replacments[$original] = '';
        if (!empty($data['phone-field-value']['country_code'])) {
          $country_info = phone_countrycodes($data['phone-field-value']['country_code']);
          if (!empty($country_info)) {
            if ($name=='country_name') {
              $replacements[$original] = $country_info['country'];
            }
            else {
              $replacements[$original] = $country_info['number'];
            }
          }
        }
      }
      else {
          $replacements[$original] = isset($data['phone-field-value'][$name]) ? $data['phone-field-value'][$name] : '';
      }
    }
  }

  return $replacements;
}
